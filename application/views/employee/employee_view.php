<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		  <div class="row">
		  	   <div class="col-lg-12 py-2">
				  	   	<span class="h4">Employee List</span>
				  	   <button class="btn btn-success float-right" data-toggle="modal" data-target="#add_emp">Add</button>
		  	   </div>
		  </div>
		  <div class="row">
		  	  <div class="col-lg-12">
		  	  	 <div id="success-message" class="alert alert-success" style="display:none;"></div>
		  	  </div>
		  </div>
		  
	</div>
	<table class="table table-hover">
		  	  	  	 <thead>
		  	  	  	 	   <tr>
		  	  	  	 	   	   <th>Emp Id</th>
		  	  	  	 	   	   <th>Firstname</th>
		  	  	  	 	   	   <th>Lastname</th>
		  	  	  	 	   	   <th>Email</th>
		  	  	  	 	   	   <th>Address</th>
		  	  	  	 	   	   <th>Mobile</th>
		  	  	  	 	   	   <th>Action</th>
		  	  	  	 	   </tr>
		  	  	  	 </thead>
		  	  	  	 <tbody id="emp-list">
		  	  	  	 	
		  	  	  	 </tbody>
		  	  	  </table>
		  	  	  <div id="pagination_link"></div>

	<div class="modal fade In" id="add_emp">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					  <h3>Add Employee</h3>
					  <button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					  <form action="" id="add_form">
					  	 <div class="form-group">
					  	 	<label for="">Firstname:</label>
					  	 	<input type="text" class="form-control" id="fname" name="fname">
					  	 	<div id="fname-error" class="text-danger"></div> 
					  	 </div>
					  	 
					  	 <div class="form-group">
					  	 	<label for="">Lastname:</label>
					  	 	<input type="text" class="form-control" id="lname" name="lname"> 
					  	 	<div id="lname-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Email:</label>
					  	 	<input type="email" class="form-control" id="email" name="email"> 
					  	 	<div id="email-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Address:</label>
					  	 	<textarea name="address" id="address" cols="30" rows="10" placeholder="Address" class="form-control"></textarea> 
					  	 	<div id="email-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Mobile:</label>
					  	 	<input type="text" class="form-control" id="mobile" placeholder="Mobile Number" maxlength="10" name="mobile"> 
					  	 	<div id="mobile-error" class="text-danger"></div> 

					  	 </div>
					  	 <div class="form-group">
					  	 	<button class="btn btn-primary btn-block">Add Employee</button> 
					  	 </div>
					  </form>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal fade In" id="edit-emp">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					  <h3>Edit Employee</h3>
					  <button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					  <form action="" id="edit_employee_form">
					  	 <div class="form-group">
					  	 	<label for="">Firstname:</label>
					  	 	<input type="text" class="form-control" id="efname" name="efname">
					  	 	<div id="fname-error" class="text-danger"></div> 
					  	 </div>
					  	 
					  	 <div class="form-group">
					  	 	<label for="">Lastname:</label>
					  	 	<input type="text" class="form-control" id="elname" name="elname"> 
					  	 	<div id="lname-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Email:</label>
					  	 	<input type="email" class="form-control" id="editemail" name="editemail"> 
					  	 	<div id="edit-email-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Address:</label>
					  	 	<textarea name="eaddress" id="eaddress" cols="30" rows="10" placeholder="Address" class="form-control"></textarea> 
					  	 	<div id="edit-email-error" class="text-danger"></div> 
					  	 </div>
					  	 <div class="form-group">
					  	 	<label for="">Mobile:</label>
					  	 	<input type="text" class="form-control" id="emobile" placeholder="Mobile Number" maxlength="10" name="emobile"> 
					  	 	<div id="emobile-error" class="text-danger"></div> 

					  	 </div>
					  	 <div class="form-group">
					  	 	  <input type="hidden" name="emp_id" id="emp_id">
					  	 </div>
					  	 <div class="form-group">
					  	 	<button class="btn btn-primary btn-block">Edit Employee</button> 
					  	 </div>
					  </form>
				</div>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script>
		  $(function(){
		  	 load_employee(1);
		  	 check_validations();
		  	 check_email();
		  	 delete_employee();
		  	  function check_email(){
		  	  	  $('#email').on('blur',function(){
		  	  	  	 let email=$(this).val();

		  	  	  	 if(email !=''){
                  $.ajax({
                  	 url:'<?php echo base_url(); ?>Appsoft_emp/check_email',
                  	 method:'post',
                  	 data:{email:email},
                  	 success:function(data){
                  	 	  if(data == 1){
                  	 	  $('#email-error').html(data);
                  	 	  $('#email').val('');
                  	 	  }
                  	 }
                  })  
		  	  	  	 }else{
		  	  	  	 	 $('#email-error,#edit-email-error').html('Email required');
		  	  	  	 }
		  	  	  })
		  	  } 
		  	  function check_validations(){
		  	  	  $('#fname').on('keyup',function(){
		  	  	  	 let fname=$(this).val();
		  	  	  	 if(fname!='' && fname.length < 5){
                   $('#fname-error').html("Firstname should be at least 5 characters long");  
		  	  	  	 }else if(!fname.match(/^[a-zA-Z ]*$/)){
                   $('#fname-error').html('Only Alphabets allowed');
		  	  	  	 }else{
		  	  	  	 	  $('#fname-error').html('');
		  	  	  	 }   
		  	  	  });
		  	  	  $('#lname').on('keyup',function(){
		  	  	  	 let lname=$(this).val();
		  	  	  	 if(lname!='' && lname.length < 5){
                   $('#lname-error').html("Lastname should be at least 5 characters long");  
		  	  	  	 }else if(!lname.match(/^[a-zA-Z ]*$/)){
                   $('#lname-error').html('Only Alphabets allowed');
		  	  	  	 }else{
		  	  	  	 	  $('#lname-error').html('');
		  	  	  	 }   
		  	  	  });
		  	  	  $('#address').on('keyup',function(){
		  	  	  	 let address=$(this).val();
		  	  	  	 if(address==''){
		  	  	  	 	 $('#address-error').html('Please fill the Address'); 
		  	  	  	 }else{
		  	  	  	 	$('#address-error').html('');
		  	  	  	 }
		  	  	  });
		  	  	  $('#mobile').on('keyup',function(){
		  	  	  	 let mobile=$(this).val();
		  	  	  	 if(!mobile.match(/^[0-9]*$/)){
		  	  	  	 	$('#mobile-error').html('Please enter digits');
		  	  	  	 	$('#mobile').val('');
		  	  	  	 }else{
		  	  	  	 	$('#mobile-error').html('');
		  	  	  	 }
		  	  	  });
		  	  } 
		  	 $(document).on('submit','#add_form',function(e){
		  	 	  e.preventDefault();
		  	 	  let form_data=$(this).serialize();
		  	 	  $.ajax({
		  	 	  	url:'<?php echo base_url(); ?>Appsoft_emp/add_employee',
		  	 	  	method:'post',
		  	 	  	data:form_data,
		  	 	  	success:function(data){
		  	 	  		$('#add_form').trigger('reset');
		  	 	  		$('#add_emp').modal('hide');
		  	 	  		$('#success-message').html('Employee Added').slideDown().delay(4000).slideUp();
		  	 	  		load_employee(1);
		  	 	  	}
		  	 	  })		  	 	  
		  	 })

		  	 function load_employee(page){
       	  	$.ajax({
       	  		url:'<?php echo base_url(); ?>Appsoft_emp/get_all_employee/'+page,
       	  		method:'post',
              dataType:'json',
       	  		success:function(data){
       	  			  $('#emp-list').html(data.employees);
                  $('#pagination_link').html(data.pagination_link);
       	  		}
       	  	})
       	  }
          //code to take offset from pagination
          $(document).on('click','.pagination li a',function(e){
             e.preventDefault();
             let page=$(this).data('ci-pagination-page');
             load_employee(page);
          });

         //Edit category
          $(document).on('click','.update',function(){
              let emp_id=$(this).attr('id');
              $.ajax({
                url:'<?php echo base_url(); ?>Appsoft_emp/get_user_details',
                method:'post',
                data:{emp_id:emp_id},
                dataType:'json',
                success:function(data){
                	// console.log(data);
                  // $('#edit-emp').modal('show');
                   $('#efname').val(data.firstname);
                  $('#elname').val(data.lastname);
                  $('#editemail').val(data.email);
                  $('#eaddress').val(data.address);
                  $('#emobile').val(data.mobile);
                  // //alert(data.category);
                  $('#emp_id').val(emp_id);
                }
              })
            });

            //Edit employee form
            $(document).on('submit','#edit_employee_form',function(e){
               e.preventDefault();
               let form_data=$(this).serialize();
               $.ajax({
               	url:'<?php echo base_url(); ?>Appsoft_emp/update_employee',
               	method:'post',
               	data:form_data,
               	success:function(data){
               			$('edit_employee_form').trigger('reset');
               			$('#edit-emp').modal('hide');
		  	 	  		$('#success-message').html('Employee Updated').slideDown().delay(4000).slideUp();
		  	 	  		  load_employee(1);
               		}
               	
               })    
            })
            //Delete employee
          
          function delete_employee(){
              $(document).on('click','.delete',function(e){
                let id=$(this).attr('id');
                if(id !=''){
                  if(confirm('Do you want to delete this category permanently?')){
                     $.ajax({
                      url:'<?php echo base_url(); ?>Appsoft_emp/remove_employee',
                      method:'post',
                      data:{id:id},
                      success:function(data){
                        if(data==1){
                        	$('#success-message').html('Employee Deleted').slideDown().delay(4000).slideUp();
		  	 	  		          load_employee(1);
                        }
                      }
                     })
                  }  
                }
              })  
             }
		  })
	</script>  	
</body>
</html>