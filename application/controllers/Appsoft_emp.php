<?php 

class Appsoft_emp extends CI_Controller{

	  public function index(){
	  	$data['title']="Employees";
	  	$this->load->view('employee/employee_view',$data);
	  }

	  public function add_employee(){
	  	  $data=[
          'firstname'=>$this->input->post('fname'), 
          'lastname'=>$this->input->post('lname'),
          'email'=>$this->input->post('email'),
          'address'=>$this->input->post('address'), 
          'mobile'=>$this->input->post('mobile'), 
	  	  ];
	  	  $add_employee=$this->Employee_model->add_employee($data);
	  	  if($add_employee){
	  	  	 echo 1;
	  	  }
	  }

	  //function for checking email
	  public function check_email(){
	  	  $email=$this->input->post('email');
	  	  $check=$this->Employee_model->check_email($email);
	  	  if($check){
	  	  	echo 'Email already registered';
	  	  }
	  }

	  //Function to get all categories
	  public function get_all_employee(){
	  	$config['base_url']=base_url('Appsoft_emp');
	  	$config['per_page']=10;
	  	$config['total_rows']=$this->Employee_model->count_employee();
	  	$config['uri_segment']=3;
	  	$config['use_page_numbers']=True;
	  	$config['full_tag_open']='<ul class="pagination">';
	  	$config['full_tag_close']='</ul>';
	  	$config['first_tag_open']='<li>';
	  	$config['first_tag_close']='</li>';
	  	$config['last_tag_open']='<li>';
	  	$config['last_tag_close']='</li>';
	  	$config['next_link']='&gt;';
	  	$config['next_tag_open']='<li>';
	  	$config['next_tag_close']='</li>';
	  	$config['prev_link']='&lt;';
	  	$config['prev_tag_open']='<li>';
	  	$config['prev_tag_close']='</li>';
	  	$config['cur_tag_open']='<li class="active"><a href="#">';
	  	$config['cur_tag_close']='</a></li>';
	  	$config['num_tag_open']='<li>';
	  	$config['num_tag_close']='</li>';
	  	$config['num_links']=1;
	  	// $config=[
    //     'base_url'=>base_url('http://localhost/Appsoft_emp/'),
    //     'per_page'=>10,
    //     'total_rows'=>$this->Employee_model->count_employee(),
    //     'uri_segment'=>3,
    //     'use_page_numbers'=>TRUE,
	  	// ];
	  	$this->pagination->initialize($config);
	  	$page=$this->uri->segment(3);
	  	$start=($page - 1) * $config['per_page'];
	  	$output =array(
         'pagination_link'=>$this->pagination->create_links(),
         'employees'=> $this->Employee_model->get_employee($config['per_page'],$start)
	  	);
	  	echo json_encode($output);
	  }

	  //Function to fetch user details
	  public function get_user_details(){
	  	$emp_id=$this->input->post('emp_id');
	  	$get_details=$this->Employee_model->get_user_details($emp_id);
	  	foreach($get_details as $row){
         $data['firstname']=$row->firstname;
         $data['lastname']=$row->lastname;
         $data['email']=$row->email;
         $data['address']=$row->address;
         $data['mobile']=$row->mobile;
	  	}
	  	echo json_encode($data);   
	  }

	  public function update_employee(){
	  	 $id=$this->input->post('emp_id');
	  	 $update=$this->Employee_model->update_employee($id);
	  	 if($update){
	  	 	echo 1;
	  	 } 
	  }

	  public function remove_employee(){
     	   $id=$this->input->post('id');
     	   $result=$this->Employee_model->delete_employee($id);
     	   if($result){
     	   	 echo 1;
     	   }
     }


}
?>